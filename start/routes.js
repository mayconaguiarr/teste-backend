"use strict";

const Route = use("Route");

Route.post("/register", "UserController.register")
Route.post("/authenticate", "UserController.authenticate");
Route.put("/authenticate/:id", "UserController.update");
Route.delete("/authenticate/:id", "UserController.destroy");

Route.group(() => {
  Route.post("/movies", "MovieController.store").middleware(["admin"]);
  Route.put("/movies/:id", "MovieController.vote")
  Route.get("/movies", "MovieController.index")
  Route.get("/movies/:id", "MovieController.getInfos")
  Route.post("/artist", "ArtistController.store").middleware(["admin"]);
  Route.post("/movie-artistic", "MovieArtistController.store").middleware(["admin"]);
  Route.post("/votes/:id", "VoteController.store")
}).middleware(["auth"]);
