const joi = require('@hapi/joi');

const artistSchema = joi.object({
  artistName: joi.string().required(),
  profession: joi.string().valid('Actor', 'Director', 'Cinematographic', 'Other').required(),
  genderActor: joi.string().valid('M', 'F', 'OTHER').required(),
  age:joi.number().integer().min(5).max(120).required(),
  city:joi.string(),
});


module.exports = artistSchema;
