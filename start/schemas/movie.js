const joi = require('@hapi/joi');

const movieSchema = joi.object({
  movieName: joi.string().required(),
  director: joi.string().required(),
  genderMovie: joi.string().required(),
  sumary: joi.string()
})


module.exports = movieSchema;
