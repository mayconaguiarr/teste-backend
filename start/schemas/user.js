const joi = require('@hapi/joi');

const userSchema = joi.object({
    username: joi.string().required(),
    email: joi.string().required(),
    password: joi.string().required(),
    isAdmin: joi.string()
});


module.exports = userSchema;
