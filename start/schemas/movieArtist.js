const joi = require('@hapi/joi');

const movieArtistSchema = joi.object({
  movieId: joi.string().required(),
  artistId: joi.string().required()
});


module.exports = movieArtistSchema;
