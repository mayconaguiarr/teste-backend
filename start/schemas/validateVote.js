const joi = require('@hapi/joi');

const validateVoteSchema = joi.object({
    votes: joi.string().valid('1', '2', '3', '4').required(),
});


module.exports = validateVoteSchema;
