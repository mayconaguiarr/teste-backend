
const { test, trait } = use('Test/Suite')('05-DeleteUser')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */

const Database = use('Database')

trait('Test/ApiClient')

test('it should delete user', async ({ assert, client }) => {
  const users = await Database
    .select('*')
    .from('users')

  const response = await client
    .delete('/authenticate/'+ users[0].id)
    .end()

  response.assertStatus(200)
  assert.exists(response.body)
})



