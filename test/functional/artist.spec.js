const { test, trait } = use('Test/Suite')('03-Artist')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */


const sessionPayloadCredentials = {
  email: 'maycon@maycon.com',
  password: '12345'
}

const Database = use('Database')

trait('Test/ApiClient')

test('it should creation artist with sucess', async ({ assert, client }) => {
  const payload = {
    artistName: 'Johny',
    profession: 'Actor',
    genderActor: 'M',
    age: 40
  }

  const responseAuth = await client
    .post('/authenticate')
    .send(sessionPayloadCredentials)
    .end()

  const response = await client
    .post('/artist')
    .send(payload)
    .header('Authorization', 'Bearer ' + responseAuth.body.token)
    .end()

  response.assertStatus(200)

  assert.exists(response.body)
})


