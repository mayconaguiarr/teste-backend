const { test, trait } = use('Test/Suite')('04-Movie')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */

const sessionPayloadCredentials = {
  email: 'maycon@maycon.com',
  password: '12345'
}

const Database = use('Database')

trait('Test/ApiClient')

test('it should creation movie with sucess', async ({ assert, client }) => {
  const payload = {
    movieName: 'Batman',
    director: 'Nollan',
    genderMovie: 'Ação',
  }

  const responseAuth = await client
    .post('/authenticate')
    .send(sessionPayloadCredentials)
    .end()

  const response = await client
    .post('/movies')
    .send(payload)
    .header('Authorization', 'Bearer ' + responseAuth.body.token)
    .end()

  response.assertStatus(200)

  assert.exists(response.body)
})

test('it should creation movieArtist with sucess', async ({ assert, client }) => {
  const responseAuth = await client
    .post('/authenticate')
    .send(sessionPayloadCredentials)
    .end()

  const payload = {
    movieId: '1',
    artistId: '1'
  }

  const response = await client
    .post('/movie-artistic')
    .send(payload)
    .header('Authorization', 'Bearer ' + responseAuth.body.token)
    .end()

  response.assertStatus(200)

  assert.exists(response.body)
})

test('it should creation votes with sucess', async ({ assert, client }) => {
  const responseAuth = await client
    .post('/authenticate')
    .send(sessionPayloadCredentials)
    .end()

  const payload = {
    votes: '1',
  }

  const movies = await Database
    .select('*')
    .from('movies')

  const response = await client
    .post('/votes/'+ movies[0].id)
    .send(payload)
    .header('Authorization', 'Bearer ' + responseAuth.body.token)
    .end()

  response.assertStatus(200)

  assert.exists(response.body)
})

test('it should get all movies with sucess', async ({ assert, client }) => {
  const query = {
    movieName: 'Batman',
    directorMovie: 'Nollan',
    genderMovie: 'Ação',
    artistName : "Johny"
  }

  const responseAuth = await client
    .post('/authenticate')
    .send(sessionPayloadCredentials)
    .end()

  const response = await client
    .get(`/movies?moviename=${query.movieName}&directorMovie=${query.directorMovie}
    &genderMovie=${query.genderMovie}&artistName=${query.artistName}`)
    .query('')
    .header('Authorization', 'Bearer ' + responseAuth.body.token)
    .end()

  response.assertStatus(200)

  assert.exists(response.body)
})

test('it should get by id movies with sucess', async ({ assert, client }) => {
  const responseAuth = await client
    .post('/authenticate')
    .send(sessionPayloadCredentials)
    .end()

  const movies = await Database
    .select('*')
    .from('movies')

  const response = await client
    .get('/movies/'+ movies[0].id)
    .header('Authorization', 'Bearer ' + responseAuth.body.token)
    .end()

  response.assertStatus(200)

  assert.exists(response.body)
})



