const { test, trait } = use('Test/Suite')('02-Session')

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */

const Database = use('Database')

trait('Test/ApiClient')

test('it should create user with success', async ({ assert, client }) => {
  const sessionPayload = {
    username: "maycon",
    email: "maayconaguiar11@hotmail.com",
    password: "12345"
  }

  const response = await client
    .post('/register')
    .send(sessionPayload)
    .end()

  response.assertStatus(200)

  assert.exists(response.body)
})


test('it should return bearer token', async ({ assert, client }) => {
  const sessionPayload = {
    email: 'maycon@maycon.com',
    password: '12345',
  }

  const response = await client
    .post('/authenticate')
    .send(sessionPayload)
    .end()

  response.assertStatus(200)
  assert.exists(response.body.token)
})

test('it should update user', async ({ assert, client }) => {
  const sessionPayload = {
    username: 'maycon1'
  }

  const users = await Database
    .select('*')
    .from('users')

  const response = await client
    .put('/authenticate/'+users[0].id)
    .send(sessionPayload)
    .end()

  response.assertStatus(200)
  assert.exists(response.body)
})





