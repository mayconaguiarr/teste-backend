# Adonis API application

![swagger](swagger.jpg)

## Production

[Production url for register](https://my-adonis-app-api.herokuapp.com/register)

## Setup Para rodar local

```js
yarn install
```

Install the adonis

```js
yarn add global @adonisjs/cli
```

### Docker - Subir banco de dados localmente

```js
docker-compose up db
```

### Migrations

Run the following command to run startup migrations.

```js
yarn migrate
```

### Run 

```js
yarn dev
```

### Test

```js
yarn test
```

### Lint

```js
yarn lint
```

### Documentation

Na pasta /docs encontra-se documentação das apis do projeto.

### Business Rules

Ao se registrar e se logar o usuário comum pode acessar as informações sobre os filmes da plataforma, buscar por todos os filmes cadastrados ou fazer filtros por ator, nome do filme, gênero do filme e diretor. Ele também pode votar nos seus filmes, podendo escolher uma nota de 1 a 4, e podendo votar somente uma vez em um filme. E depois de algum filme ter no mínimo um voto, o usuário já pode consultar informações detalhadas de um filme específico, e a nota que o filme possui. A nota é calculada a partir do (somátorio de todas as notas que os usuários deram) / (dividido pela quantidade de notas atribuidas). Ex: (1 + 2 + 3 + 4/ 4) = 2.5

O usuário admin após se registrar e logar, ele deve cadastrar filmes, artistas, se é ator,
diretor e etc. E deve relacionar qual ator fez qual filme a partir dos ids de filmes e artistas já cadastrados anteriormente.
Ambos os usuários podem editar sua conta ou até mesmo excluir. O usuário admin é cadastrado enviando um isAdmin no body da api de registro, sendo um campo opcional quando não é enviado o usuário não tem os privilégios de admin.
