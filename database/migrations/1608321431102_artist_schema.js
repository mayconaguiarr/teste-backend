'use strict'

const Schema = use('Schema')

class ArtistsSchema extends Schema {
  up () {
    this.create('artists', (table) => {
      table.increments()
      table.string('artist_name', 60).notNullable()
      table.string('profession', 60).notNullable()
      table.string('gender_actor').notNullable()
      table.string('age').notNullable()
      table.string('city')
      table
        .integer('user_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('users')
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
      table.timestamps()
    })
  }
  down () {
    this.drop('artists')
  }
}

module.exports = ArtistsSchema

