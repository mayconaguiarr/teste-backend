'use strict'

const Schema = use('Schema')

class Movie_ArtistsSchema extends Schema {
  up () {
    this.create('movie_artists', (table) => {
      table.increments()
      table
        .integer('movie_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('movies')
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
      table
        .integer('artist_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('artists')
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
        table
          .integer('user_id')
          .unique()
          .unsigned()
          .notNullable()
          .references('id')
          .inTable('users')
          .onUpdate("CASCADE")
          .onDelete("CASCADE");
      table.timestamps()
    })
  }
  down () {
    this.drop('movie_artists')
  }
}

module.exports = Movie_ArtistsSchema

