'use strict'

const Schema = use('Schema')

class MoviesSchema extends Schema {
  up () {
    this.create('movies', (table) => {
      table.increments()
      table.string('movie_name', 60).notNullable()
      table.string('director',60).notNullable()
      table.string('gender_movie',40).notNullable()
      table.string('sumary')
      table
        .integer('user_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('users')
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
      table.timestamps()
    })
  }
  down () {
    this.drop('movies')
  }
}

module.exports = MoviesSchema
