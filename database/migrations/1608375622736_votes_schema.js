'use strict'

const Schema = use('Schema')

class VotesSchema extends Schema {
  up () {
    this.create('votes', (table) => {
      table.increments()
      table.string('votes')
      table
        .integer('movie_id')
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('movies')
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
      table
        .integer('user_id')
        .unique()
        .unsigned()
        .notNullable()
        .references('id')
        .inTable('users')
        .onUpdate("CASCADE")
        .onDelete("CASCADE");
      table.timestamps()
    })
  }
  down () {
    this.drop('votes')
  }
}

module.exports = VotesSchema

