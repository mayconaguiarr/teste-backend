'use strict'

const Model = use('Model')

class Artist extends Model {
  movie() {
    return this.hasMany("App/Models/Movie");
  }
}

module.exports = Artist
