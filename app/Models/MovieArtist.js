'use strict'

const Model = use('Model')

class MovieArtist extends Model {
  movie() {
    return this.belongsToMany("App/Models/Artist");
  }
  artist(){
    return this.belongsToMany("App/Models/Movie");
  }
}

module.exports = MovieArtist
