"use strict";

const Hash = use("Hash");
const Model = use("Model");

class User extends Model {
  static boot() {
    super.boot();

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook("beforeSave", async userInstance => {
      if (userInstance.dirty.password) {
        userInstance.password = await Hash.make(userInstance.password);
      }
    });
  }

  tokens() {
    return this.hasMany("App/Models/Token");
  }
  artists() {
    return this.hasMany("App/Models/Artist");
  }
  movies() {
    return this.hasMany("App/Models/Movie");
  }
  movieArtists() {
    return this.hasMany("App/Models/MovieArtist");
  }
  votes() {
    return this.hasMany("App/Models/Vote");
  }

}

module.exports = User;
