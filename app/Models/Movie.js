'use strict'

const Model = use('Model')

class Movie extends Model {
  artist() {
    return this.hasMany("App/Models/Artist");
  }
}

module.exports = Movie
