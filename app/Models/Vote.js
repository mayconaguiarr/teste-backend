'use strict'

const Model = use('Model')

class Vote extends Model {
  movie() {
    return this.belongsTo("App/Models/Movie");
  }


}

module.exports = Vote
