"use strict";

const User = use("App/Models/User");
const validator = require('../../../start/schemas/user');

class AuthController {
  async register({ request, response }) {
    const data = request.body;
    const error = validator.validate(data)

    const insert = await ({ username: data.username,
      email: data.email, password: data.password, is_admin: data.isAdmin?  data.isAdmin: ''
    });

    if(error.error){
      return response.status(400).send({
        success: false,
        message: error.error,
      });
    }

    const user = await User.create(insert);

    return user;
  }

  async authenticate({ request, auth }) {
    const { email, password } = request.all();

    const token = await auth.attempt(email, password);

    return token;
  }

  async update ({ params, request }) {
    const data = request.body;

    const user = await User.findOrFail(params.id)

    user.merge(data)

    await user.save()

    return user
  }

  async destroy ({ params }) {
    const user = await User.findOrFail(params.id)

    const data = await user.delete()

    return data;
  }
}

module.exports = AuthController;
