"use strict";

const Artist = use("App/Models/Artist");
const validator = require('../../../start/schemas/artist');

class ArtistController {
  async store({ request, response, auth }) {
    const data = request.body

    const insert = await ({ user_id: auth.user.id,  artist_name: data.artistName,
      profession: data.profession, gender_actor: data.genderActor, age: data.age
    });

    const error = validator.validate(data)
    await ({ user_id: auth.user.id, ...data });

    if(error.error){
      return response.status(400).send({
        success: false,
        message: error.error,
      });
    }

    const artist = await Artist.create(insert);

    return artist;
  }

}

module.exports = ArtistController;
