"use strict";

const MovieArtist = use("App/Models/MovieArtist");
const validator = require('../../../start/schemas/movieArtist');

class MovieArtisticController {
  async store({ request, response, auth }) {
    const data = request.body

    const insert = await ({ user_id: auth.user.id,  movie_id: data.movieId, artist_id: data.artistId});

    const error = validator.validate(data)

    if(error.error){
      return response.status(400).send({
        success: false,
        message: error.error,
      });
    }

    const movieArtist = await MovieArtist.create(insert);

    return movieArtist;
  }
}

module.exports = MovieArtisticController;
