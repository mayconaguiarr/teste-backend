"use strict";

const Movie = use("App/Models/Movie");
const validator = require('../../../start/schemas/movie');
const Database = use('Database')

class AuthController {
  async store({ request, response, auth }) {
    const data = request.body;
    const error = validator.validate(data)

    const insert = await ({ user_id: auth.user.id,  movie_name: data.movieName,
      director: data.director, gender_movie: data.genderMovie, sumary: data.sumary
    });

    if(error.error){
      return response.status(400).send({
        success: false,
        message: error.error,
      });
    }

    const movie = await Movie.create(insert);

    return movie;
  }

  async index({ request, auth }) {
    const movie = request.input('movie')

    if(movie) {var movieName = `movieName LIKE '%${movieName}%' AND`}

    const director = request.input('director')

    if(director) {var directorMovie = `director LIKE '%${director}%' AND`}

    const gender = request.input('genderMovie')

    if(gender) {var genderMovie = `gender_movie LIKE '%${gender}%' AND`}

    const artist = request.input('artistName')

    if(artist) {var artistName = `a.artist_name LIKE '%${artist}%' AND`}

    await ({ user_id: auth.user.id});

    if(!(movie|| director|| gender|| artist)) {
      var posts = await Database
        .select('m.movie_name as nameMovie', ' m.director as director','a.artist_name as artistName',
       'm.gender_movie as genderMovie').from('movie_artists as ma')
       .innerJoin('movies as m', 'ma.movie_id', 'm.id')
       .innerJoin('artists as a', 'a.id', 'ma.artist_id')
      return posts
    }else{
      posts = await Database
        .raw(`
          SELECT m.movie_name as nameMovie, m.director as director,
          a.artist_name as artistName, m.gender_movie as genderMovie FROM movie_artists ma
          JOIN movies m ON (ma.movie_id = m.id)
          JOIN artists a ON (a.id = ma.artist_id)
          WHERE ${!movieName? '': movieName}
           ${!directorMovie? '': directorMovie}
           ${!genderMovie? '': genderMovie}
           ${!artistName? '': artistName} m.id IS NOT NULL
        `)
        return posts[0]
    }
  }

  async getInfos({ params, auth }) {
    const movie = await Movie.findOrFail(params.id)
    await ({ user_id: auth.user.id});

    var votesCount = await Database
      .select('*')
      .from('votes')
      .count('id as total')

    var votes = await Database
      .select('*')
      .from('votes')
      .where('movie_id', '=', params.id)

    var cont = 0
    for(var i = 0; i < votes.length; i++) {
      cont += parseFloat(votes[i].votes)
    }

    const allMovieData = {
      movieName: movie.$attributes.movie_name,
      voteMedia: cont/votesCount[0].total,
      genderMovie: movie.$attributes.gender_movie,
      sumary: movie.$attributes.sumary
    }

    return allMovieData
  }
}

module.exports = AuthController;
