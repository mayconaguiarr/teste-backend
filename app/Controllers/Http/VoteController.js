"use strict";

const Vote = use("App/Models/Vote");
const validator = require('../../../start/schemas/validateVote');

class VoteController {
  async store({ request, response, auth, params }) {
    const data = request.body

    const insert = await ({ user_id: auth.user.id,  votes: data.votes, movie_id: params.id});

    const error = validator.validate(data)
    await ({ user_id: auth.user.id, ...data });

    if(error.error){
      return response.status(400).send({
        success: false,
        message: error.error,
      });
    }

    const vote = await Vote.create(insert);

    return vote;
  }
}

module.exports = VoteController;
